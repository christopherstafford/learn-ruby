class Ingredient
  attr_accessor :type, :amount, :daily_use

  def needed
    return @daily_use * 7
  end
end

